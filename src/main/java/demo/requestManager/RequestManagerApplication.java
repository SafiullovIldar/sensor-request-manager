package demo.requestManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Точка входа в приложение.
 */
@PropertySource(value = {
	"classpath:custom.properties",
	"classpath:application.properties"}, encoding = "UTF-8")
@SpringBootApplication
public class RequestManagerApplication {

	/**
	 * Точка входа в приложение.
	 * @param args дополнительные аргументы.
	 */
	public static void main(String[] args) {
		SpringApplication.run(RequestManagerApplication.class, args);
	}

}
