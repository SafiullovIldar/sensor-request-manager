package demo.requestManager.resources;

import demo.requestManager.dto.ContainerDto;
import demo.requestManager.services.KafkaService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Контроллер для получения показаний датчиков на контейнерах от модуля emulator.
 */
@RestController
@RequiredArgsConstructor
@Validated
public class SensorRequestController {

    /**
     * Сервис для взаимодействия с kafka. */
    @NonNull private final KafkaService kafkaService;

    /**
     * Получение
     * @param containerDto дто с информацией о контейнере.
     */
    @PostMapping("/new_indication_sensors")
    public void getContainerFromScheduler(@Valid @RequestBody @NonNull final ContainerDto containerDto) {
        kafkaService.sendContainerInfoInTopic(containerDto);
    }
}
