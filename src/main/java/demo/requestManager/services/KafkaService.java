package demo.requestManager.services;

import demo.requestManager.dto.ContainerDto;

/**
 * Сервис для взаимодействия с kafka.
 */
public interface KafkaService {

    /**
     * Отправляет показания датчиков на контейнере в kafka topic.
     * @param containerDto дто с информацией о контейнере.
     */
    void sendContainerInfoInTopic(ContainerDto containerDto);
}
