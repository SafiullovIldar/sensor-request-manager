package demo.requestManager.services;

import demo.requestManager.dto.ContainerDto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Сервис для взаимодействия с kafka.
 */
@Service
@RequiredArgsConstructor
public class KafkaServiceImpl implements KafkaService {

    /**
     * Выводит информацию в лог. */
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaServiceImpl.class);

    /**
     * Класс для взаимодействия с kafka. */
    @NonNull private final KafkaTemplate<String, ContainerDto> kafkaTemplate;

    /**
     * Название топика. */
    @Value(value = "${topic.name.sensors}")
    private String sensorTopicName;

    @Override
    public void sendContainerInfoInTopic(@NonNull final ContainerDto containerDto) {
        kafkaTemplate.send(sensorTopicName, containerDto);
        LOGGER.info("Отправлено в Kafka " + containerDto);
    }
}
